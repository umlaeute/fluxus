; draw a row of cubes, scaled 
; by the harmonics from the sound input

(define num-bars 32)

(define (bars c)
    (cond ((not (negative? c))
        (translate (vector 1.1 0 0))
        (with-state
            (colour (vector 1 0.1 (- 1 (gh c))))
            (scale (vector 1 (+ 0.1 (* 2 (gh c))) 1))
            (draw-cube))
        (bars (- c 1)))))

(clear)
(stop-audio)

; centeral
(translate (vector (- -0.5 (/ (* 1.1 num-bars) 2)) 0 0))

;(start-audio "fluxa:Out0" 1024 44100)
;(start-audio "alsa_pcm:capture_1" 1024 44100)
(start-audio "system:capture_1" 1024 44100)
;(start-audio "PulseAudio JACK Sink:front-left" 1024 44100)

(set-num-frequency-bins num-bars)
(every-frame (bars (- num-bars 1)))
